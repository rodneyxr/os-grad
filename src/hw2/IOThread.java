package hw2;

public class IOThread extends Thread {
	private boolean finished = false;

	private ProcessQueue readyQueue;
	private ProcessQueue ioQueue;

	public IOThread(ProcessQueue readyQueue, ProcessQueue ioQueue) {
		this.readyQueue = readyQueue;
		this.ioQueue = ioQueue;
	}

	@Override
	public void run() {

		while (!finished) {

			// pick the first process in the queue
			Process proc = ioQueue.peek();
			if (proc == null) {
				try {
					// idle
					Thread.sleep(1);
				} catch (InterruptedException e) {
				}
				continue;
			}

			// do I/O burst
			int burst = proc.work();
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
			}

			// if finished, move to readyQueue
			if (burst == 0) {
				ioQueue.poll();
				if (proc.isHalted()) {
					System.err.println("Error: Process cannot move from WAITING -> HALTED.");
					System.exit(1);
				} else {
					readyQueue.add(proc);
				}
			}

		} // END_FINISHED

	}

	public void finish() {
		finished = true;
	}

}
