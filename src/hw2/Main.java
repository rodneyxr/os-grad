package hw2;

/**
 * This is a multithreaded program that will allow us to measure the performance
 * (i.e., CPU utilization, throughput, turnaround time, waiting time, and
 * response time) of the four basic CPU scheduling algorithms (namely, FIFO,
 * SJF, PR, and RR) by simulating the processes whose priority, sequence of CPU
 * burst time(ms) and I/O burst time(ms) will be given in an input file.
 * 
 * @author Rodney Rodriguez
 *
 */
public class Main {

	public static void main(String[] args) {
		String alg = null;
		int quantum = 0;
		String filename = null;

		if (args.length < 2 || !args[0].equals("-alg")) {
			printUsageAndExit();
		}

		alg = args[1];
		alg = alg.toUpperCase().trim();
		if (!alg.matches("FIFO|SJF|PR|RR")) {
			printUsageAndExit();
		}

		if (alg.equalsIgnoreCase("RR")) {
			if (!args[2].equals("-quantum")) {
				System.out.println("RR requires -quantum to be set!");
				printUsageAndExit();
			}
			try {
				quantum = Integer.parseInt(args[3]);
			} catch (NumberFormatException e) {
				System.out.println("RR requires -quantum [integer(ms)] to be set!");
				printUsageAndExit();
			}
			if (quantum <= 0) {
				System.err.println("Error: Quantum must be greater than 0!");
				System.exit(1);
			}
		}

		if (!args[args.length - 2].equals("-input")) {
			printUsageAndExit();
		}

		filename = args[args.length - 1];

		// Initialize the queues
		ProcessQueue readyQueue = new ProcessQueue();
		ProcessQueue ioQueue = new ProcessQueue();

		// create the input thread
		InputThread inputThread = new InputThread(filename, readyQueue);

		// create the I/O system thread
		IOThread ioThread = new IOThread(readyQueue, ioQueue);

		// create the cpu scheduler thread
		CPUThread cpuThread = new CPUThread(CPUThread.Alg.valueOf(alg), quantum, readyQueue, ioQueue);

		// start the input thread
		inputThread.start();
		// start the CPU scheduler
		cpuThread.start();
		// start the I/O system thread
		ioThread.start();

		int nProcs = 0;
		try {
			// wait for input thread to finish
			inputThread.join();

			// CPU and I/O will not finish until all processes have halted
			nProcs = inputThread.getNumberOfProcesses();
			Global.S.acquire(nProcs);
			cpuThread.finish();
			ioThread.finish();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		float waitTime = 0;
		float tat = 0;
		float responseTime = 0;
		for (Process p : Global.PROCESSES) {
			waitTime += p.waitTime;
			tat += p.getTurnaroundTime();
			responseTime += p.getResponseTime();
		}

		waitTime /= (float) nProcs;
		tat /= (float) nProcs;
		responseTime /= (float) nProcs;

		System.out.printf("Input File Name		: %s\n", filename);
		System.out.printf("CPU Scheduling Alg	: %s\n",
				alg + (alg.equalsIgnoreCase("RR") ? " " + String.valueOf(quantum) : ""));
		System.out.printf("CPU Utilization 	: %.2f\n", (float) CPUThread.CPU / (CPUThread.CPU + cpuThread.idle));
		System.out.printf("Throughput		: %.2f\n", (float) nProcs / CPUThread.CPU);
		System.out.printf("Turnaround time 	: %.2f\n", tat);
		System.out.printf("Waiting time 		: %.2f\n", waitTime);
		System.out.printf("Response time 		: %.2f\n", responseTime);
	}

	// prog -alg [FIFO|SJF|PR|RR] [-quantum [integer(ms)]] -input [file name]
	private static void printUsageAndExit() {
		System.err.println("Usage: prog -alg [FIFO|SJF|PR|RR] [-quantum [integer(ms)]] -input [file name]");
		System.exit(1);
	}

}
