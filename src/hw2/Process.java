package hw2;

import java.util.List;

public class Process {

	int waitTime = 0;
	private int completedTime = -1;
	int startTime = -1;
	int work = 0;

	private int priority;
	private int arrivalTime;
	private List<Integer> bursts;

	public Process(int priority, int arrivalTime, List<Integer> bursts) {
		setPriority(priority);
		this.arrivalTime = arrivalTime;
		this.bursts = bursts;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		if (priority < 1) {
			System.err.println("Error: Process priority cannot be less than 1.");
			System.exit(1);
		}
		if (priority > 10) {
			System.err.println("Error: Process priority cannot be greater than 10.");
			System.exit(1);
		}
		this.priority = priority;
	}

	public int getArrivalTime() {
		return arrivalTime;
	}

	// tat = t(process completed) - t(process submitted)
	public int getTurnaroundTime() {
		return completedTime - arrivalTime;
	}

	// rt = t(first response) - t(submission of request)
	public int getResponseTime() {
		return startTime - arrivalTime;
	}

	public boolean isHalted() {
		return bursts.isEmpty();
	}

	public synchronized int getBurstTime() {
		if (bursts.isEmpty())
			return -1;
		return bursts.get(0);
	}

	public synchronized int work() {
		if (bursts.isEmpty())
			return 0;
		int burst = bursts.get(0);
		burst--;
		work++;
		if (burst > 0)
			bursts.set(0, burst);
		else
			bursts.remove(0);
		return burst;
	}

	public void terminate(int completedTime) {
		this.completedTime = completedTime;
		Global.PROCESSES.add(this);
		Global.S.release();
	}

}
