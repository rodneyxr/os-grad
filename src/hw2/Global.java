package hw2;

import java.util.ArrayList;
import java.util.concurrent.Semaphore;

public class Global {
	public static final ArrayList<Process> PROCESSES = new ArrayList<Process>();
	public static final Semaphore S = new Semaphore(0);
}
