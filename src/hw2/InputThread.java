package hw2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class InputThread extends Thread {
	private String inputFilename;
	private ProcessQueue readyQueue;
	private int numberOfProcesses = 0;

	public InputThread(String inputFileName, ProcessQueue readyQueue) {
		this.inputFilename = inputFileName;
		this.readyQueue = readyQueue;
	}

	public int getNumberOfProcesses() {
		return numberOfProcesses;
	}

	@Override
	public void run() {
		try (BufferedReader br = new BufferedReader(new FileReader(inputFilename))) {
			String s;
			while ((s = br.readLine()) != null) {
				s = s.toLowerCase().trim();
				String[] cmd = s.split("\\s");

				switch (cmd[0]) {
				case "proc":
					// this process has no work so skip it
					if (cmd.length < 3)
						continue;
					try {
						// parse priority (1-10)
						int priority = Integer.parseInt(cmd[1]);
						List<Integer> bursts = Collections.synchronizedList(new ArrayList<Integer>());
						for (int i = 2; i < cmd.length; i++) {
							bursts.add(Integer.parseInt(cmd[i]));
						}
						// create the process and add it to the ready queue
						Process proc = new Process(priority, CPUThread.CPU, bursts);
						synchronized (readyQueue) {
							readyQueue.add(proc);
							numberOfProcesses++;
						}
					} catch (NumberFormatException e) {
						System.err.println("Error: " + e.getMessage());
						System.exit(1);
					}
					break;

				case "sleep":
					int sleepTime = 0;
					try {
						sleepTime = Integer.parseInt(cmd[1]);
						Thread.sleep(sleepTime);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					break;

				case "stop":
					return;

				default:
					if (cmd[0].length() > 0) {
						System.err.printf("Error: Invalid input '%s'.\n", cmd[0]);
						System.exit(1);
					}
					break;
				}
			}
		} catch (FileNotFoundException e) {
			System.err.printf("Error: File '%s' not found.\n", inputFilename);
			System.exit(1);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
