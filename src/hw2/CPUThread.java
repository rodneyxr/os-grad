package hw2;

public class CPUThread extends Thread {
	public static int CPU = 0;

	public enum Alg {
		FIFO, SJF, PR, RR
	};

	private boolean finished = false;

	private ProcessQueue readyQueue;
	private ProcessQueue ioQueue;

	private Alg alg;
	private final int quantum;

	public CPUThread(Alg alg, int quantum, ProcessQueue readyQueue, ProcessQueue ioQueue) {
		this.alg = alg;
		this.quantum = quantum;
		this.readyQueue = readyQueue;
		this.ioQueue = ioQueue;
	}

	int idle;

	@Override
	public void run() {
		CPUThread.CPU = 0;
		int burst = 0;
		idle = 0;
		Process proc = null;
		int q = 0; // current quantum

		while (!finished) {

			if (proc == null) {
				switch (alg) {
				case FIFO:
					// pick the first process in the queue
					proc = readyQueue.peek();
					break;
				case SJF:
					// pick the process with the smallest burst time
					proc = readyQueue.peek();
					if (proc == null)
						break;
					for (Process p : readyQueue) {
						if (p.getBurstTime() < proc.getBurstTime())
							proc = p;
					}
					break;
				case PR:
					// pick the process with the highest priority
					proc = readyQueue.peek();
					if (proc == null)
						break;
					for (Process p : readyQueue) {
						if (p.getPriority() > proc.getPriority())
							proc = p;
					}
					break;
				case RR:
					proc = readyQueue.peek();
					q = quantum;
					break;
				default:
					System.err.println("Error: Unknown scheduling algorithm!");
					System.exit(1);
					break;
				}
			}

			if (proc == null) {
				try {
					idle++;
					Thread.sleep(1);
				} catch (InterruptedException e) {
				}
				continue;
			}

			// save the start time
			if (proc.startTime == -1)
				proc.startTime = CPUThread.CPU;

			// make the process work and decrement it's cpu burst
			burst = proc.work();
			try {
				Thread.sleep(1);
				CPUThread.CPU++;
			} catch (InterruptedException e) {
			}

			// increment all processes' wait counts
			for (Process p : readyQueue) {
				if (p == proc) // skip this process; it is not waiting
					continue;
				p.waitTime++;
			}

			// if the process' burst is done remove it from the queue
			if (burst == 0) {
				readyQueue.remove(proc);
				// if there is no more work for the process terminate it
				if (proc.isHalted()) {
					proc.terminate(CPUThread.CPU);
				} else {
					// else add it to the I/O queue
					ioQueue.add(proc);
				}
				proc = null;
			} else if (alg == Alg.RR) {
				q--;
				if (q == 0) {
					// quantum expired
					// move the process to the end
					readyQueue.poll();
					readyQueue.add(proc);
					proc = null;
				}
			}

		} // END_FINISHED
	}

	public void finish() {
		finished = true;
	}

}
