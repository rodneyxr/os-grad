#!/bin/sh
PROMPT='>>'
INPUT=$1

if [ -z "$1"  ]
then
    echo "Please supply an input file to test!"
    exit
fi

echo "$PROMPT cat $INPUT"
cat $INPUT
echo

echo "$PROMPT ./run -alg FIFO -input '$INPUT'"
./run -alg FIFO -input \'$INPUT\'
echo

echo "$PROMPT ./run -alg SJF -input '$INPUT'"
./run -alg SJF -input \'$INPUT\'
echo

echo "$PROMPT ./run -alg PR -input '$INPUT'"
./run -alg PR -input \'$INPUT\'
echo

echo "$PROMPT ./run -alg RR -quantum 1 -input '$INPUT'"
./run -alg RR -quantum 1 -input \'$INPUT\'
echo

echo "$PROMPT ./run -alg RR -quantum 3 -input '$INPUT'"
./run -alg RR -quantum 3 -input \'$INPUT\'
echo

echo "$PROMPT ./run -alg RR -quantum 5 -input '$INPUT'"
./run -alg RR -quantum 5 -input \'$INPUT\'
echo
