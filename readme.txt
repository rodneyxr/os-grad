Java Version: Java 8

Import into eclipse.

or

Use the build/run perl scripts in the root of the project.

or
	
Compile
	mkdir bin
	javac -cp . -d bin src/hw2/*
	
Run
	java -cp bin hw2.Main -alg [FIFO|SJF|PR|RR] [-quantum [integer(ms)]] -input [file name]

